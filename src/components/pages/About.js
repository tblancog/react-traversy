import React from 'react'
import { Fragment } from 'react'

const About = () => {
  return (
    <Fragment>
      <h1>About this App</h1>
      <p>App to search Gihub users</p>
      <p>Version: 1.0.0</p>
    </Fragment>
  )
}

export default About
